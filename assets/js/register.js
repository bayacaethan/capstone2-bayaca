let registerForm = document.querySelector("#registerUser")


registerForm.addEventListener("submit", (e) => {



	e.preventDefault()

	console.log("I triggered the submit event")

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value


	if((password1 !== '' && password2 !== '' ) && (password1 === password2) && (mobileNo.length === 11)){

		// fetch( url , options )
		fetch('https://intense-gorge-81273.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email
			})
			// "{email: email}"
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if (data === false) {

				fetch('https://intense-gorge-81273.herokuapp.com/api/users', {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if(data === true){
						alert("registered successfully")

						//redirect to login page
						window.location.replace("./login.html")
					} else {
						// error in creating registration
						alert("something went wrong")
					}

				})

			}

		})

	}

})
