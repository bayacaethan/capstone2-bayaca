let params =new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token= localStorage.getItem('token')

fetch(`https://intense-gorge-81273.herokuapp.com/api/courses/${courseId}`, {
	method: "PUT",
	headers: {
		"Authorization" : `Bearer ${token}`
	}
})

.then(res => res.json())
.then(data => {
	console.log(data)
	if (data) {

		window.location.replace('./courses.html')
	} else {
		alert('Something went wrong')
	}
})