console.log(window.location.search)


let params = new URLSearchParams(window.location.search);


console.log(params.has('courseId'))


let courseId = params.get('courseId')
console.log(courseId);

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")

fetch(`https://intense-gorge-81273.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	console.log(data)

	name.placeholder = data.name
	price.placeholder = data.price
	description.placeholder = data.description
	name.value = data.name
	price.value = data.price
	description.value = data.description

})

document.querySelector("#editCourse").addEventListener("submit", (e) => {

	e.preventDefault()

	let courseName = name.value
	let desc = description.value
	let priceValue = price.value

	let token = localStorage.getItem('token')

	fetch('https://intense-gorge-81273.herokuapp.com/api/courses', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            id: courseId,
            name: courseName,
            description: desc,
            price: priceValue
        })
    })
    .then(res => res.json())
    .then(data => {
        
    	console.log(data)

    	if(data === true){
    	
    	    window.location.replace("./courses.html")
    	}else{
    	    alert("something went wrong")
    	}

    })

})

