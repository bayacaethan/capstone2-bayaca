let params = new URLSearchParams(window.location.search);

let courseId = params.get("courseId");

let adminUser = localStorage.getItem("isAdmin");

let token = localStorage.getItem("token");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");


fetch(`https://intense-gorge-81273.herokuapp.com/api/courses/${courseId}`)
  .then((res) => res.json())
  .then((data) => {
    courseName.innerHTML = data.name;
    courseDesc.innerHTML = data.description;
    coursePrice.innerHTML = data.price;
    console.log(data);
    if (adminUser == "false" && token != null) {
      enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`;

      document.querySelector("#enrollButton").addEventListener("click", () => {

        fetch("https://intense-gorge-81273.herokuapp.com/api/users/enroll", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            courseId: courseId,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            if (data === true) {
              alert("Thank you for enrolling to the course.");
              window.location.replace("./courses.html");
            } else {
              alert("Something Went Wrong.");
            }
          })
          .catch((err) => {
            alert("Course has been already enrolled");
          });
      });
    } else if (token != null) {
      if (data.enrollees.length < 1) {
        enrollContainer.innerHTML = "No Enrollees Available";
      } else {
        fetch("https://intense-gorge-81273.herokuapp.com/api/users/")
          .then((res) => res.json())
          .then((users) => {
            data.enrollees.forEach((enrollee) => {
              users.forEach((user) => {
                if (enrollee.userId === user._id) {
                  enrollContainer.innerHTML += `

							<div class="card">
								<div class="card-body">
									<h5 class="card-title">${user.firstName} ${user.lastName}</h5>
									<p class="card-text text-center">${enrollee.enrolledOn}</p>
								</div>
							</div>

					`;
                }
              });
            });
          });
      }
    } else {
      enrollContainer.innerHTML = `<a href="./register.html" id="enrollButton" class="btn btn-block btn-primary">Enroll</a>`;
    }
  });
