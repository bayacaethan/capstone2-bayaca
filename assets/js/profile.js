let name = document.querySelector("#userName");
let desc = document.querySelector("#userDesc");
let userCourses = document.querySelector("#enrollContainer");
let editButton=document.querySelector("#editButton");
let token = localStorage.getItem('token');

let courseArray = [];


fetch(`https://intense-gorge-81273.herokuapp.com/api/users/details`,{

	headers: {
		'Authorization' : `Bearer ${token}`
	}
	
})
.then(res => res.json())
.then(data =>{


	if (data) {

		name.innerHTML =` ${data.firstName}  ${data.lastName}`
		desc.innerHTML =`Mobile Number: ${data.mobileNo}  Email: ${data.email}`
		editButton.innerHTML=`
			<div class="col-md-2 offset-md-10">
				<a href="./editProfile.html?userId=${data._id}" value="{data._id}" class="btn btn-block btn-outline-info">Edit Profile</a>
			</div>
		`
	} else {

		alert("Something went wrong");
	}


	data.enrollments.forEach(course => {
		console.log(course)
	})  

	

	data.enrollments.forEach(course => {


		let courseId = course.courseId


		fetch(`https://intense-gorge-81273.herokuapp.com/api/courses/${courseId}`,{

			headers: {
				"Content-Type": 'application/json',
				'Authorization': `Bearer ${token}`
			}

		})
		.then(res => res.json())
		.then(data =>{		
			console.log(data, "data")	
		
			if (data) {
				userCourses.innerHTML +=
				` 
					<div class="card my-5">
						<div class="card-body">
							<h5 class="card-title">${data.name}</h5>
							<h5 class="card-title">${course.enrolledOn}</h5>
							<p class="card-title">${course.status}</p>


					</div>
				`
				

			} else {
				alert("Something went wrong");
			}





		})

	})	

})