let params = new URLSearchParams(window.location.search);
let userId = params.get("userId");


let fName = document.querySelector('#firstName');
let lName = document.querySelector('#lastName');
let mobileNo = document.querySelector('#mobileNumber');
let email = document.querySelector('#userEmail');
let pass1=document.querySelector("#password1");
let pass2= document.querySelector("#password2");
let password;

fetch(`https://intense-gorge-81273.herokuapp.com/api/users/details/${userId}`)
.then(res => res.json())
.then(data => {
    // console.log(data)

    fName.placeholder = data.firstName;
    lName.placeholder = data.lastName;
    mobileNo.placeholder = data.mobileNo;
    email.placeholder = data.email;

    fName.value = data.firstName;
    lName.value = data.lastName;
    mobileNo.value = data.mobileNo;
    email.value = data.email;



    document.querySelector('#editProfile').addEventListener("submit", (e) => {
        e.preventDefault()

        let firstn = fName.value;
        let lastn = lName.value;
        let mobileNum = mobileNo.value;
        let userEmail = email.value;
        // password1.value = data.password

        if (pass1.value === '' && pass2.value === ''){
        password = data.password
        } else if (pass1.value === pass2.value){
        password =pass1.value

        }
        let token = localStorage.getItem('token');

        fetch('https://intense-gorge-81273.herokuapp.com/api/users', {
            method: "PUT",
            headers:{

                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                id: userId,
                firstName: firstn,
                lastName: lastn,
                email: userEmail,
                mobileNo: mobileNum,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                alert('Profile has been edited');
                window.location.replace('./profile.html');
            }else {
                alert('Something went wrong.')
            }
        })
    })
})

